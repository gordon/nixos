# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ lib, config, pkgs, ... }:

let
  nixos-unstable = import <unstable> {};
  export_x_info = (
    pkgs.writeTextFile {
      name = "export_x_info";
      destination = "/bin/export_x_info";
      executable = true;
      text = ''
      #!${pkgs.bash}/bin/bash
      # Export the dbus session address on startup so it can be used by cron
      touch $HOME/.Xdbus
      chmod 600 $HOME/.Xdbus
      env | grep DBUS_SESSION_BUS_ADDRESS > $HOME/.Xdbus
      echo 'export DBUS_SESSION_BUS_ADDRESS' >> $HOME/.Xdbus
      '';
      }
    );
  swayrun = (
      pkgs.writeTextFile {
        name = "swayrun";
        destination = "/bin/swayrun";
        executable = true;
        text = ''
          #! ${pkgs.bash}/bin/bash

          eval $(${pkgs.gnome.gnome_keyring}/bin/gnome-keyring-daemon --start --components=pkcs11,secrets)
          ${export_x_info}/bin/export_x_info
          ${pkgs.sway}/bin/sway
        '';
      }
    );
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # <nixpkgs/nixos/modules/profiles/hardened.nix>
      <home-manager/nixos>
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.supportedFilesystems = [ "zfs" ];
  boot.cleanTmpDir = true;
  boot.kernelParams = [ "amdgpu.dc=0" ];

  networking = {
    hostName = "nixos"; # define your hostname
    hostId = "aaaaaa";
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.

    nat.enable = true;
    nat.internalInterfaces = [ "ve-+" ];
    nat.externalInterface = "wlp1s0";

    networkmanager.enable = true;
    networkmanager.unmanaged = [ "interface-name:ve-*" ];
    
    nameservers = [ "80.67.169.12" "2001:910:800::12" "80.67.169.40" "2001:910:800::40" ];
  };

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp3s0.useDHCP = true;


   security.wrappers = {
     pmount.source = "${pkgs.pmount}/bin/pmount"; 
     pmount.owner = "root";
     pmount.group = "root";
     pumount.source = "${pkgs.pmount}/bin/pumount";
     pumount.owner = "root";
     pumount.group = "root";
   };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";
  console = {
    # font = "Lat2-Terminus16";
    font = with pkgs; "${powerline-fonts}/share/fonts/psf/ter-powerline-v22b.psf.gz";
    keyMap = "fr-bepo";
  };

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.pulseaudio = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    neovim firefox-wayland vlc nextcloud-client keepassxc xfce.thunar xfce.tumbler chromium gnome3.seahorse libreoffice evince zathura gnome3.gnome_keyring cava
    feh transmission transmission-remote-gtk picard meld youtube-dl audacity mplayer simple-scan xdg_utils tor-browser-bundle-bin abcde dia
    xdg_utils abcde dia weechat
    gimp inkscape darktable kdenlive audacity shntool flac cuetools ffmpeg-full obs-studio
    thunderbird signal-desktop mumble tdesktop
    veracrypt
    gimp inkscape darktable kdenlive audacity shntool flac cuetools ffmpeg-full
    nixos-unstable.obs-studio
    qjackctl ardour guitarix shntool flac cuetools ffmpeg-full
    breeze-qt5 breeze-gtk breeze-icons numix-solarized-gtk-theme numix-icon-theme numix-cursor-theme lxappearance qt5ct xorg.xev xorg.xeyes
    notmuch-mutt isync notmuch msmtp youtube-dl ponysay cava borgbackup
    ripgrep bat exa fzf ctags lynx pdfpc youtube-dl mplayer wget htop
    gitAndTools.gitFull file strace tmux gotop neofetch unzip p7zip mercurial nmon nmap ranger broot zip fd powertop mosh
    autoPatchelfHook nox meson ninja signify home-manager
    gnumake pam_u2f tree libsecret gcc urlscan firejail dnsutils jmtpfs pmount
    cryptsetup usbutils lshw acpi inetutils libnotify ldns httpie borgbackup
    ntfs3g
    ansible pypy nodejs jetbrains.pycharm-community rustup cargo rustc clippy pkgconfig nix-prefetch-scripts atom
    texlive.combined.scheme-full pandoc
    adoptopenjdk-icedtea-web virt-viewer tcpdump tshark sqlite
    (python27.withPackages(ps: with ps; [  ]))
    (python3.withPackages(ps: with ps; [ pip requests rq psutil tkinter
    virtualenvwrapper flake8 mypy debugpy jupyter psutil ipython autopep8 black
    pylint ]))
    mpd ncmpcpp pavucontrol
    steam lutris wine vulkan-loader steam-run vulkan-tools
    (
      pkgs.writeTextFile {
        name = "startsway";
        destination = "/bin/startsway";
	executable = true;
        text = ''
          #! ${pkgs.bash}/bin/bash
          
          # first import invironment variables from the login manager
          systemctl --user import-environment
          # then start the service
          exec systemctl --user start sway.service
        '';
      }
    )
    swayrun export_x_info
  ];

  virtualisation.docker.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 631 6600 8888 51413 ];
  networking.firewall.allowedUDPPorts = [ 631 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.gutenprint pkgs.hplip pkgs.cnijfilter2 ];
  services.printing.browsing = true;
  services.printing.listenAddresses = [ "127.0.0.1:631" "192.168.1.10:631" ];
  services.printing.defaultShared = true;

  # Enable sound.
  sound.enable = true;
  sound.mediaKeys.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;

  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  hardware.u2f.enable = true;
  services.redis.enable = true;

  security.pam.u2f = {
    enable = true;
    control = "required";
    cue = true;
  };

  hardware.sane.enable = true;
  # Enable the X11 windowing system.
  services.xserver = {
    enable = false;
    autorun = false;
    displayManager.startx.enable = true;
    layout = "fr";
    xkbVariant = "bepo";
    xkbOptions = "eurosign:e";
    displayManager.sddm.enable = false;
    desktopManager.plasma5.enable = false;
  };

  # Enable touchpad support.
  services.xserver.libinput = {
    enable = true;
    touchpad.tapping = true;
  };

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  services.zfs.trim.enable = true;

  services.gnome.gnome-keyring.enable = true;

  security.pam.services.login.enableGnomeKeyring = true;
  security.pam.services.login.enableKwallet = true;
  
  security.pam.services.kwallet = {
    name = "kwallet";
    enableKwallet = true;
  };

  services.ntp.enable = true;

  services.avahi = {
    enable = true;
    publish.enable = true;
    publish.userServices = true;
    nssmdns = true;
  };

  services.openvpn.servers.faimaison.config = "config etc/nixos/openvpn/faimaison.conf";

  # services.tlp.enable = true;
  # services.tlp.settings = {
  #   USB_AUTOSUSPEND = 1;
  #   USB_BLACKLIST = "1050:0407";
  #   RUNTIME_PM_BLACKLIST="\"03:00.4 06:00.3 06:00.4\"";
  # };

  # powerManagement.resumeCommands = "systemctl resstart openvpn-faimaison.service";
  environment = {
    etc."openvpn/passFMA".source = ./openvpn/passFMA;
    pathsToLink = [ "/share/zsh" ];
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.

  users.users.gaelle = {
    isNormalUser = true;
    extraGroups = [ "gaelle" "wheel" "sway" "video" "audio" "disk" "networkmanager" "adbusers" "scanner" "docker" ]; 
    shell = pkgs.zsh;
  };

  users.users.formation = {
    isNormalUser = true;
    extraGroups = [ "formation" "wheel" "video" "audio" "disk" "networkmanager" "docker" ];
    shell = pkgs.zsh;
  };

  # home-manager.users.root = import ./hm-root.nix;

  hardware.opengl = {
    driSupport32Bit = true;
    extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];
  };

  hardware.steam-hardware.enable = true;

  hardware.pulseaudio.support32Bit = true;

  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    liberation_ttf
    fira-code
    fira-code-symbols
    nerdfonts
    powerline-fonts
    font-awesome_4
    siji
    symbola
  ];

  fonts.fontconfig.defaultFonts.monospace = [
    "Fira Code Medium"
    "Symbola"
    "IPAGothic"
  ];

  programs = {
    mtr.enable = true;
    adb.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
      pinentryFlavor = "qt";
    };
    qt5ct.enable = true;
    sway = {
      enable = true;
      extraPackages = with pkgs; [
        swaylock
        swayidle
        xwayland
        waybar
        mako
        kanshi
        kitty
        rofi
        grim
        slurp
      ];
    }; 
    # zsh.enable = true;
    # zsh.promptInit = "export POWERLEVEL9K_MODE=nerdfont-complete; source ${pkgs.zsh-powerlevel9k}/share/zsh-powerlevel9k/powerlevel9k.zsh-theme";
    # zsh.ohMyZsh = {
    #   enable = true;
    #   plugins = [ "git" "git-extras" "python" "man" "tmux" ];
    #   theme = "agnoster";
    #   customPkgs = with pkgs; [ nix-zsh-completions zsh-powerlevel9k ];
    # };
  };

  systemd.user.targets.sway-session = {
    description = "Sway compositor session";
    documentation = [ "man:systemd.special(7)" ];
    bindsTo = [ "graphical-session.target" ];
    wants = [ "graphical-session-pre.target" ];
    after = [ "graphical-session-pre.target" ];
  };

  systemd.user.services.sway = {
    description = "Sway - Wayland window manager";
    documentation = [ "man:sway(5)" ];
    bindsTo = [ "graphical-session.target" ];
    wants = [ "graphical-session-pre.target" ];
    after = [ "graphical-session-pre.target" ];
    # We explicitly unset PATH here, as we want it to be set by
    # systemctl --user import-environment in startsway
    environment.PATH = lib.mkForce null;
    serviceConfig = {
      Type = "simple";
      ExecStart = ''
        ${pkgs.dbus}/bin/dbus-run-session ${swayrun}/bin/swayrun
      '';
      Restart = "on-failure";
      RestartSec = 1;
      TimeoutStopSec = 10;
    };
  };

  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "gaelle" ];
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "19.09"; # Did you read the comment?

}

